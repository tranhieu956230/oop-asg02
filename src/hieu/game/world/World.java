package hieu.game.world;

import hieu.game.Handler;
import hieu.game.entities.Entity;
import hieu.game.entities.creature.Player;
import hieu.game.tiles.Tile;
import hieu.game.tiles.TileManager;
import hieu.game.utils.Utils;

import java.awt.*;

public class World {
    TileManager tileManager;
    private int width, height;
    private int spawnX, spawnY;
    private Handler handler;
    private int[][] tiles;
    private Entity entity;

    public World(Handler handler, String path) {
        this.handler = handler;
        tileManager = new TileManager(handler);
        entity = new Entity(handler);
        loadWorld(path);
    }

    private void loadWorld(String path) {
        //WorldGenerator.generateWorld(tileManager, "res/worlds/world2.txt", 30, 30);

        String file = Utils.loadTextAsString(path);

        String[] tokens = file.split("\\s+");
        width = Utils.parseInt(tokens[0]);
        height = Utils.parseInt(tokens[1]);
        spawnX = Utils.parseInt(tokens[2]);
        spawnY = Utils.parseInt(tokens[3]);

        Entity player = new Player(handler, spawnX, spawnY);
        entity.addEntity(player);
        tiles = new int[height][width];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                tiles[y][x] = Utils.parseInt(tokens[x + y * width + 4]);
            }
        }
    }

    public Tile getTile(int x, int y) {
        return tileManager.getTile(tiles[y][x]);
    }

    public void tick() {
        entity.tick();
    }

    public void render(Graphics g) {
        int yStart, xStart, xEnd, yEnd;
        xStart = Math.max(0, handler.getGameCamera().getxOffset() / Tile.TILE_WIDTH);
        xEnd = Math.min(width, (handler.getGameCamera().getxOffset() + handler.getGame().getDisplay().getWidth()) / Tile.TILE_WIDTH + 1);
        yStart = Math.max(0, handler.getGameCamera().getyOffset() / Tile.TILE_HEIGHT );
        yEnd = Math.min(height, (handler.getGameCamera().getyOffset() + handler.getGame().getDisplay().getHeight()) / Tile.TILE_HEIGHT + 1);

        for (int y = yStart; y < yEnd; y++) {
            for (int x = xStart; x < xEnd; x++) {
                getTile(x, y).render(g, x * Tile.TILE_WIDTH, y * Tile.TILE_HEIGHT);
            }
        }
        entity.render(g);
    }

    public void setTile(int x, int y, int code) {
        tiles[y][x] = code;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
