package hieu.game.world;

import hieu.game.tiles.Tile;
import hieu.game.tiles.TileManager;

import java.io.*;
import java.util.ArrayList;

public class WorldGenerator {

    public static void generateWorld(TileManager tileManager, String path, int width, int height) {
        try {
            StringBuilder sb = new StringBuilder();
            File file = new File(path);
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            ArrayList<Integer> tileIds = new ArrayList<>();
            Tile[] tiles = tileManager.getTiles();
            for (int i = 0; i < tiles.length; i++) {
                if(tiles[i] != null) tileIds.add(tiles[i].getId());
            }

            sb.append(width + " " + height + "\n");
            sb.append(100 + " " + 100 + "\n");

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int number = (int) Math.floor(Math.random() * tileIds.size());
                    int id = tileIds.get(number);
                    if(x == 0 || y == 0 || x == width - 1 || y == height - 1) id = 40;
                    sb.append(id + " ");
                }
                sb.append("\n");
            }

            writer.write(sb.toString());
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
