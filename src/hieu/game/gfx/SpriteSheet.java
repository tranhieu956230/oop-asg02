package hieu.game.gfx;

import java.awt.image.BufferedImage;

public class SpriteSheet {
    private BufferedImage sheet;
    public SpriteSheet(BufferedImage sheet) {
        this.sheet = sheet;
    }

    public BufferedImage crop(int x, int y, int width, int height) {
        return sheet.getSubimage(x, y, width, height);
    }

    public BufferedImage[] cropAll(int width, int height) {
        BufferedImage[] images = new BufferedImage[sheet.getWidth()/width];
        for(int i = 0; i < images.length; i++) {
            images[i] = crop(i * width, 0, width, height);
        }
        return images;
    }

    public BufferedImage[] cropBombSprite() {
        BufferedImage[] images = new BufferedImage[4];
        for(int i = 0; i < images.length; i++) {
            if(i == 3) images[i] = crop(i * 16 + i , 0, 15, 16);
            else images[i] = crop(i * 16, 0, 16, 16);
        }
        return images;
    }
}
