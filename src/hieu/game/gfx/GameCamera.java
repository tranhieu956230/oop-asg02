package hieu.game.gfx;

import hieu.game.Handler;
import hieu.game.tiles.Tile;

public class GameCamera {
    private int xOffset, yOffset;
    private Handler handler;

    public GameCamera(Handler handler) {
        this.handler = handler;
        xOffset = 0;
        yOffset = 0;
    }

    private void limitCamera() {
        if (xOffset < 0) xOffset = 0;
        if (yOffset < 0) yOffset = 0;
        if (xOffset > handler.getWorld().getWidth() * Tile.TILE_WIDTH - handler.getGame().getDisplay().getWidth())
            xOffset = handler.getWorld().getWidth() * Tile.TILE_WIDTH - handler.getGame().getDisplay().getWidth();
        if (yOffset > handler.getWorld().getHeight() * Tile.TILE_HEIGHT - handler.getGame().getDisplay().getHeight())
            yOffset = handler.getWorld().getHeight() * Tile.TILE_HEIGHT - handler.getGame().getDisplay().getHeight();
    }

    public void centerOnCreature(int x, int y, int width, int height) {
        xOffset = x - handler.getGame().getDisplay().getWidth() / 2 + width / 2;
        yOffset = y - handler.getGame().getDisplay().getHeight() / 2 + height / 2;
        limitCamera();
    }

    public int getxOffset() {
        return xOffset;
    }

    public int getyOffset() {
        return yOffset;
    }
}
