package hieu.game.gfx;

import java.awt.image.BufferedImage;

public class Animation {
    private int speed, index;
    private BufferedImage[] frames;
    private long lastTime, timer;

    private boolean timeout = false;

    public Animation(int speed, BufferedImage[] frames) {
        this.speed = speed;
        this.frames = frames;
        this.index = 0;
        timer = 0;
        lastTime = System.currentTimeMillis();
    }

    public BufferedImage getCurrentFrame() {
        return frames[index];
    }

    public boolean isTimeout() {
        return timeout;
    }

    public void tick() {
        timer += System.currentTimeMillis() - lastTime;
        lastTime = System.currentTimeMillis();

        if(timer > speed) {
            index++;
            timer = 0;
            if(index >= frames.length) {
                index = 0;
                timeout = true;
            }
        }
    }
}
