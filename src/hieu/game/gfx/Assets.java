package hieu.game.gfx;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Assets {
    private static final int width = 32;
    private static final int height = 32;


    public static BufferedImage[] dirt, grass, stone;
    public static BufferedImage[] player_down, player_left, player_right, player_up, player_die, player_idle;
    public static BufferedImage[] enemy_down, enemy_left, enemy_right, enemy_up, enemy_die;
    public static BufferedImage[] bomb, flame;

    public static BufferedImage background, title;
    public static int playerWidth = 17;
    public static int playerHeight = 24;
    public static final int playerScale = 3;

    public static final int bombWidth = 32, bombHeight = 32;

    public static Font font28;

    public static void init() {

        font28 = FontLoader.loadFont("res/fonts/slkscr.ttf", 28);
        bomb = new BufferedImage[3];
        flame = new BufferedImage[5];
        player_die = new BufferedImage[4];
        enemy_die = new BufferedImage[4];
        enemy_up = new BufferedImage[3];
        enemy_right = new BufferedImage[3];
        enemy_left = new BufferedImage[3];
        enemy_down = new BufferedImage[3];

        SpriteSheet dirtSheet = new SpriteSheet(ImageLoader.loadImage("/textures/dirt.png"));
        SpriteSheet grassSheet = new SpriteSheet(ImageLoader.loadImage("/textures/grass.png"));
        SpriteSheet stoneSheet = new SpriteSheet(ImageLoader.loadImage("/textures/stone.png"));
        SpriteSheet playerSheetDown = new  SpriteSheet(ImageLoader.loadImage("/textures/player/down/player_down.png"));
        SpriteSheet playerSheetUp = new  SpriteSheet(ImageLoader.loadImage("/textures/player/up/player_up.png"));
        SpriteSheet playerSheetLeft = new  SpriteSheet(ImageLoader.loadImage("/textures/player/left/player_left.png"));
        SpriteSheet playerSheetRight= new  SpriteSheet(ImageLoader.loadImage("/textures/player/right/player_right.png"));
        SpriteSheet playerSheetIdle = new SpriteSheet(ImageLoader.loadImage("/textures/player/idle/player_idle.png"));

        bomb[0] = ImageLoader.loadImage("/textures/Sprites/Bomb/Bomb_f01.png");
        bomb[1] = ImageLoader.loadImage("/textures/Sprites/Bomb/Bomb_f02.png");
        bomb[2] = ImageLoader.loadImage("/textures/Sprites/Bomb/Bomb_f03.png");

        flame[0] = ImageLoader.loadImage("/textures/Sprites/Flame/Flame_f00.png");
        flame[1] = ImageLoader.loadImage("/textures/Sprites/Flame/Flame_f01.png");
        flame[2] = ImageLoader.loadImage("/textures/Sprites/Flame/Flame_F02.png");
        flame[3] = ImageLoader.loadImage("/textures/Sprites/Flame/Flame_F03.png");
        flame[4] = ImageLoader.loadImage("/textures/Sprites/Flame/Flame_F04.png");


        player_die[0] = ImageLoader.loadImage("/textures/player/die/player_die1.png");
        player_die[1] = ImageLoader.loadImage("/textures/player/die/player_die2.png");
        player_die[2] = ImageLoader.loadImage("/textures/player/die/player_die3.png");
        player_die[3] = ImageLoader.loadImage("/textures/player/die/player_die4.png");

        enemy_die[0] = ImageLoader.loadImage("/textures/bazooka/die/bazooka_die1.png");
        enemy_die[1] = ImageLoader.loadImage("/textures/bazooka/die/bazooka_die2.png");
        enemy_die[2] = ImageLoader.loadImage("/textures/bazooka/die/bazooka_die3.png");
        enemy_die[3] = ImageLoader.loadImage("/textures/bazooka/die/bazooka_die4.png");

        enemy_down[0] = ImageLoader.loadImage("/textures/bazooka/down/bazooka_down1.png");
        enemy_down[1] = ImageLoader.loadImage("/textures/bazooka/down/bazooka_down2.png");
        enemy_down[2] = ImageLoader.loadImage("/textures/bazooka/down/bazooka_down3.png");

        enemy_up[0] = ImageLoader.loadImage("/textures/bazooka/up/bazooka_up1.png");
        enemy_up[1] = ImageLoader.loadImage("/textures/bazooka/up/bazooka_up2.png");
        enemy_up[2] = ImageLoader.loadImage("/textures/bazooka/up/bazooka_up3.png");

        enemy_left[0] = ImageLoader.loadImage("/textures/bazooka/left/bazooka_left1.png");
        enemy_left[1] = ImageLoader.loadImage("/textures/bazooka/left/bazooka_left2.png");
        enemy_left[2] = ImageLoader.loadImage("/textures/bazooka/left/bazooka_left3.png");

        enemy_right[0] = ImageLoader.loadImage("/textures/bazooka/right/bazooka_right1.png");
        enemy_right[1] = ImageLoader.loadImage("/textures/bazooka/right/bazooka_right2.png");
        enemy_right[2] = ImageLoader.loadImage("/textures/bazooka/right/bazooka_right3.png");

        background = ImageLoader.loadImage("/textures/Sprites/Menu/title_background.jpg");
        title = ImageLoader.loadImage("/textures/Sprites/Menu/title_titletext.png");


        dirt = dirtSheet.cropAll(width, height);
        grass = grassSheet.cropAll(width, height);
        stone = stoneSheet.cropAll(width, height);

        player_down = playerSheetDown.cropAll(playerWidth, playerHeight);
        player_up = playerSheetUp.cropAll(playerWidth, playerHeight);
        player_left = playerSheetLeft.cropAll(playerWidth, playerHeight);
        player_right = playerSheetRight.cropAll(playerWidth, playerHeight);
        player_idle = playerSheetIdle.cropAll(playerWidth, playerHeight);
    }


}
