package hieu.game.tiles;


import hieu.game.Handler;

public class TileManager {
    private static Tile[] tiles;
    public TileManager(Handler handler) {
        tiles = new Tile[256];
        //Stone
        addTile(new StoneTile(0, 10, handler));
        addTile(new StoneTile(1, 11, handler));

        //Grass
        addTile(new GrassTile(0, 20, handler));
        addTile(new GrassTile(1, 21, handler));
        addTile(new GrassTile(2, 22, handler));
        //Dirt
        addTile(new DirtTile(0, 30, handler));
        addTile(new DirtTile(1, 31, handler));
        addTile(new DirtTile(2, 32, handler));
        //Wall
        addTile(new WallTile(0, 40, handler));

    }

    private void addTile(Tile t) {
        tiles[t.getId()] = t;
    }

    public Tile getTile(int index) {
        if (index < 0 || index >= 256) return null;
        Tile t = tiles[index];
        if(t == null) return tiles[30];
        return t;
    }

    public Tile[] getTiles() {
        return tiles;
    }
}
