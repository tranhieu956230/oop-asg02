package hieu.game.tiles;

import hieu.game.Handler;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {
    protected BufferedImage texture;
    protected final int id;
    public static final int TILE_WIDTH = 64, TILE_HEIGHT = 64;
    private Handler handler;

    public Tile(BufferedImage texture, int id, Handler handler) {
        this.texture = texture;
        this.id = id;
        this.handler = handler;
    }
    public void tick() {

    }

    public void render(Graphics g, int x, int y) {
        g.drawImage(texture, x - handler.getGameCamera().getxOffset(), y - handler.getGameCamera().getyOffset(), TILE_WIDTH, TILE_HEIGHT, null);
    }

    public int getId() {
        return id;
    }

    public boolean isSolid() {
        return false;
    }

    public boolean isBreakable() {return false;}
}
