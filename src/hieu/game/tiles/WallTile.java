package hieu.game.tiles;

import hieu.game.Handler;
import hieu.game.gfx.Assets;

public class WallTile extends Tile {
    public WallTile(int pos, int id, Handler handler) {
        super(Assets.stone[pos], id, handler);
    }

    public boolean isSolid() {
        return true;
    }

    public boolean isBreakable() {
        return false;
    }
}
