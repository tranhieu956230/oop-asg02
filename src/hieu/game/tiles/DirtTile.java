package hieu.game.tiles;

import hieu.game.Handler;
import hieu.game.gfx.Assets;

public class DirtTile extends Tile {
    public DirtTile(int pos, int id, Handler handler) {
        super(Assets.dirt[pos], id, handler);
    }
}
