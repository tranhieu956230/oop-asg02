package hieu.game.tiles;

import hieu.game.Handler;
import hieu.game.gfx.Assets;

public class GrassTile extends Tile {
    public GrassTile(int pos, int id, Handler handler) {
        super(Assets.grass[pos], id, handler);
    }
}
