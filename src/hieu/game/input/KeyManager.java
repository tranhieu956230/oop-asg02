package hieu.game.input;

import hieu.game.Handler;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {
    private boolean[] keys = new boolean[256];
    private boolean[] cantPress, justPressed;
    private Handler handler;

    public KeyManager(Handler handler) {
        this.handler = handler;
        handler.setKeyManager(this);
        cantPress = new boolean[256];
        justPressed = new boolean[256];
    }

    public void tick() {
        for (int i = 0; i < keys.length; i++) {
            if (cantPress[i] && !keys[i]) {
                cantPress[i] = false;
            } else if (justPressed[i]) {
                cantPress[i] = true;
                justPressed[i] = false;
            }
            if (!cantPress[i] && keys[i]) {
                justPressed[i] = true;
            }
        }
    }

    public boolean isPressed(int code) {
        return keys[code];
    }

    public boolean keyJustPressed(int keyCode) {
        if (keyCode < 0 || keyCode >= keys.length) return false;
        return justPressed[keyCode];
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code < 0 || code >= keys.length) return;
        keys[code] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int code = e.getKeyCode();
        if (code < 0 || code >= keys.length) return;
        keys[code] = false;
    }
}
