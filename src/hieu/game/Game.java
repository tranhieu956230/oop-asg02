package hieu.game;

import hieu.game.display.Display;
import hieu.game.gfx.Assets;
import hieu.game.gfx.GameCamera;
import hieu.game.input.KeyManager;
import hieu.game.states.GameState;
import hieu.game.states.MenuState;
import hieu.game.states.State;
import hieu.game.world.World;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;

public class Game implements Runnable {
    //Timer variable
    private final long oneSecond = 1000000000;
    private final int fps = 144;
    BufferStrategy bufferStrategy;
    Graphics graphics;
    //
    private Display display;
    private Thread thread;
    private Handler handler;
    private State gameState;
    private State menuState;
    private KeyManager keyManager;
    private GameCamera camera;
    //Constructor variable
    private String title;
    private int width, height;
    private boolean running;

    public Game(String title, int width, int height) {
        this.title = title;
        this.width = width;
        this.height = height;
        display = new Display(title, width, height);
        handler = new Handler(this);
        keyManager = new KeyManager(handler);
        camera = new GameCamera(handler);
        handler.setGameCamera(camera);
    }

    private void init() {
        display.getCanvas().createBufferStrategy(3);
        bufferStrategy = display.getCanvas().getBufferStrategy();
        Assets.init();
        gameState = new GameState(handler);
        menuState = new MenuState(handler);
        State.setState(menuState);
        display.getFrame().addKeyListener(keyManager);
    }

    public void run() {
        init();
        long timePerFrame = oneSecond / fps;
        long timer = 0;
        long now, lastTime = System.nanoTime();
        long fpsTimer = 0;
        int frameCount = 0;

        while (running) {
            now = System.nanoTime();

            timer += now - lastTime;
            fpsTimer += now - lastTime;

            lastTime = System.nanoTime();
            if (timer >= timePerFrame) {
                tick();
                render();
                timer -= timePerFrame;
                frameCount++;
            }

            if(fpsTimer >= oneSecond) {
                System.out.println("FPS: " + frameCount);
                fpsTimer -= oneSecond;
                frameCount = 0;
            }
        }
        stop();
    }

    public State getMenuState() {
        return menuState;
    }

    public void tick() {
        keyManager.tick();
        if(State.getState() instanceof  MenuState && keyManager.keyJustPressed(KeyEvent.VK_ENTER)) {
            State.setState(gameState);
        }
        State.getState().tick();
    }

    public void render() {
        graphics = bufferStrategy.getDrawGraphics();
        graphics.clearRect(0, 0, width, height);

        State.getState().render(graphics);

        bufferStrategy.show();
        graphics.dispose();
        Toolkit.getDefaultToolkit().sync();
    }


    public synchronized void start() {
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Display getDisplay() {
        return display;
    }

    //GETTERS SETTERS

}
