package hieu.game.entities;

import hieu.game.Handler;
import hieu.game.entities.creature.Bazooka;
import hieu.game.entities.creature.Creature;
import hieu.game.entities.creature.Player;
import hieu.game.entities.statics.Bomb;
import hieu.game.entities.statics.Flame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;

public class Entity implements Comparable<Entity> {
    private static ArrayList<Entity> entities = new ArrayList<>();
    protected float xMove, yMove;
    protected float x, y;
    protected int width, height;
    protected Rectangle bounds;
    protected Handler handler;
    protected int priority;

    public Entity(Handler handler) {
        this.handler = handler;
        entities.add(new Bazooka(handler, 500, 200));
        entities.add(new Bazooka(handler, 550, 300));
        entities.add(new Bazooka(handler, 400, 400));
        entities.add(new Bazooka(handler, 500, 500));
        entities.add(new Bazooka(handler, 600, 600));
        entities.add(new Bazooka(handler, 600, 200));
        entities.add(new Bazooka(handler, 320, 300));
        entities.add(new Bazooka(handler, 430, 400));
        entities.add(new Bazooka(handler, 540, 500));
        entities.add(new Bazooka(handler, 450, 600));
        entities.add(new Bazooka(handler, 450, 200));
        entities.add(new Bazooka(handler, 620, 300));
        entities.add(new Bazooka(handler, 430, 400));
        entities.add(new Bazooka(handler, 710, 500));
        entities.add(new Bazooka(handler, 860, 600));
        entities.add(new Bazooka(handler, 660, 200));
        entities.add(new Bazooka(handler, 1030, 300));
        entities.add(new Bazooka(handler, 1220, 400));
        entities.add(new Bazooka(handler, 1210, 500));
        entities.add(new Bazooka(handler, 1200, 600));

    }

    public Entity(Handler handler, float x, float y, int width, int height, int priority) {
        this.handler = handler;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.priority = priority;
        xMove = 0;
        yMove = 0;
    }

    public Rectangle getCollisionBounds() {
        return new Rectangle((int) x + bounds.x, (int) y + bounds.y, bounds.width, bounds.height);
    }

    public Rectangle getLeftCollisionBounds() {
        return new Rectangle((int) (x + bounds.x + xMove - 1), (int) (y + bounds.y), 2, bounds.height);
    }

    public Rectangle getRightCollsiionBounds() {
        return new Rectangle((int) (x + bounds.x + xMove + bounds.width + 1), (int) (y + bounds.y), 2, bounds.height);
    }

    public Rectangle getUpCollisionBounds() {
        return new Rectangle((int) (x + bounds.x), (int) (y + bounds.y - 1 + yMove), bounds.width, 2);
    }

    public Rectangle getDownCollisionBounds() {
        return new Rectangle((int) (x + bounds.x), (int) (y + bounds.y + 1 + yMove + bounds.height), bounds.width, 2);
    }

    public boolean isCollided() {
        for (Entity e : entities) {
            if (e != this && !(e instanceof Flame)) {
                if (xMove > 0 && e.getCollisionBounds().intersects(getRightCollsiionBounds())) return true;
                if (xMove < 0 && e.getCollisionBounds().intersects(getLeftCollisionBounds())) return true;
                if (yMove < 0 && e.getCollisionBounds().intersects(getUpCollisionBounds())) return true;
                if (yMove > 0 && e.getCollisionBounds().intersects(getDownCollisionBounds())) return true;
            }
        }
        return false;
    }

    public void checkCollideWithEnemy() {
        for (Entity e : entities) {
            if ((e instanceof Bazooka) && this instanceof Player) {
                if (xMove > 0 && e.getCollisionBounds().intersects(getRightCollsiionBounds()))
                    ((Player) this).hurt(((Bazooka) e).getDamage());
                if (xMove < 0 && e.getCollisionBounds().intersects(getLeftCollisionBounds()))
                    ((Player) this).hurt(((Bazooka) e).getDamage());
                if (yMove < 0 && e.getCollisionBounds().intersects(getUpCollisionBounds()))
                    ((Player) this).hurt(((Bazooka) e).getDamage());
                if (yMove > 0 && e.getCollisionBounds().intersects(getDownCollisionBounds()))
                    ((Player) this).hurt(((Bazooka) e).getDamage());
            }
        }
    }

    public void checkFlameDamage() {
        for (Entity e1 : entities) {
            for (Entity e2 : entities) {
                if (e1 instanceof Creature && e2 instanceof Flame) {
                    if (e1.getCollisionBounds().intersects(e2.getCollisionBounds())) ((Creature) e1).hurt(Bomb.damage);
                }
            }
        }
    }

    public void addEntity(Entity e) {
        entities.add(e);
    }


    private void checkTimeoutBomb() {
        int size1 = entities.size();
        for (int i = 0; i < size1; i++) {
            if (entities.get(i) instanceof Bomb && ((Bomb) entities.get(i)).isTimeOut()) {
                ((Bomb) entities.get(i)).explode();
                entities.remove(i);
                i--;
                size1--;
            }
        }
    }

    private void checkTimeoutFlame() {
        int size1 = entities.size();
        for (int i = 0; i < size1; i++) {
            if (entities.get(i) instanceof Flame && ((Flame) entities.get(i)).isTimeOut()) {
                entities.remove(i);
                i--;
                size1--;
            }
        }
    }

    private void removeDeadCreature() {
        int size1 = entities.size();
        for (int i = 0; i < size1; i++) {
            if (entities.get(i) instanceof Creature && ((Creature) entities.get(i)).isShouldRemove()) {
                entities.remove(i);
                i--;
                size1--;
            }
        }
    }

    private boolean checkWin() {
        boolean win = true;
        int size = entities.size();
        for (int i = 0; i < size; i++) {
            if (entities.get(i) instanceof Bazooka) {
                win = false;
                break;
            }
        }
        return win;
    }

    public void tick() {
        checkFlameDamage();
        removeDeadCreature();
        checkTimeoutFlame();
        checkTimeoutBomb();

        if(checkWin()) {
            JFrame frame = handler.getGame().getDisplay().getFrame();
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        }

        int length = entities.size();
        for (int i = 0; i < length; i++) {
            entities.get(i).tick();
        }
    }

    public void render(Graphics g) {
        //Collections.sort(entities);
        int length = entities.size();
        for (int i = 0; i < length; i++) {
            entities.get(i).render(g);
        }
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public int compareTo(Entity e) {
        if (priority > e.priority) return 1;
        return -1;
    }
}
