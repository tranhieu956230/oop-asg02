package hieu.game.entities.creature;

import hieu.game.Handler;
import hieu.game.entities.statics.Bomb;
import hieu.game.gfx.Animation;
import hieu.game.gfx.Assets;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Bazooka extends Creature {
    private final int scale = Assets.playerScale;
    Animation moveUp, moveLeft, moveRight, moveDown, idle, die;
    private boolean up, down, right, left;
    private BufferedImage texture;

    private long lastTime, moveTime;

    public Bazooka(Handler handler, float x, float y) {
        super(handler, x, y, Assets.playerWidth * Assets.playerScale, Assets.playerHeight * Assets.playerScale, 2);
        speed = 1;
        int xBound = 4 * scale;
        int yBound = 10 * scale;
        int boundWidth = 9 * scale;
        int boundHeight = 13 * scale;
        bounds = new Rectangle(xBound, yBound, boundWidth, boundHeight);
        moveUp = new Animation(200, Assets.enemy_up);
        moveDown = new Animation(200, Assets.enemy_down);
        moveLeft = new Animation(200, Assets.enemy_left);
        moveRight = new Animation(200, Assets.enemy_right);
        idle = new Animation(1000, Assets.enemy_down);
        die = new Animation(700, Assets.enemy_die);
        lastTime = System.currentTimeMillis();
        moveTime = 0;
        damage = 2;
    }

    public void tick() {
        if (!died) {
            idle.tick();
            moveUp.tick();
            moveDown.tick();
            moveLeft.tick();
            moveRight.tick();
            setDirection();
            updateSpeed();
            move();
        } else {
            die.tick();
            if(die.isTimeout()) {
                shouldRemove = true;
            }
        }
        if(!shouldRemove) updateFrame();
    }


    public void pollDirection() {
        up = false;
        down = false;
        left = false;
        right = false;
        switch ((int) Math.floor(Math.random() * 4)) {
            case 0:
                up = true;
                break;
            case 1:
                down = true;
                break;
            case 2:
                left = true;
                break;
            case 3:
                right = true;
                break;
        }
        lastTime = System.currentTimeMillis();
        moveTime = (long) (Math.random() * 5 * 1000 + 1000);
    }

    public void changeDirection() {
        pollDirection();
    }

    private void setDirection() {
        if (System.currentTimeMillis() - lastTime > moveTime) {
            pollDirection();
        }
    }

    public void updateFrame() {
        if (died) {
            texture = die.getCurrentFrame();
        } else if (up) {
            texture = moveUp.getCurrentFrame();
        } else if (down) {
            texture = moveDown.getCurrentFrame();
        } else if (left) {
            texture = moveLeft.getCurrentFrame();
        } else if (right) {
            texture = moveRight.getCurrentFrame();
        } else {
            texture = idle.getCurrentFrame();
        }
    }

    public void updateSpeed() {
        xMove = 0;
        yMove = 0;
        if (up) {
            yMove = -speed;
        } else if (down) {
            yMove = speed;
        } else if (left) {
            xMove = -speed;
        } else if (right) {
            xMove = speed;
        }
    }

    public void render(Graphics g) {
        g.drawImage(texture, (int) x - handler.getGameCamera().getxOffset(), (int) y - handler.getGameCamera().getyOffset(), width, height, null);
    }

}
