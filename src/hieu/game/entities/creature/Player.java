package hieu.game.entities.creature;

import hieu.game.Handler;
import hieu.game.entities.statics.Bomb;
import hieu.game.gfx.Animation;
import hieu.game.gfx.Assets;
import hieu.game.states.State;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

public class Player extends Creature {
    private final int scale = Assets.playerScale;
    Animation moveUp, moveLeft, moveRight, moveDown, idle, die;
    private boolean up, down, right, left;
    private BufferedImage texture;

    public Player(Handler handler, float x, float y) {
        super(handler, x, y, Assets.playerWidth * Assets.playerScale, Assets.playerHeight * Assets.playerScale, 2);
        speed = 5;
        int xBound = 4 * scale;
        int yBound = 10 * scale;
        int boundWidth = 9 * scale;
        int boundHeight = 13 * scale;
        bounds = new Rectangle(xBound, yBound, boundWidth, boundHeight);
        moveUp = new Animation(200, Assets.player_up);
        moveDown = new Animation(200, Assets.player_down);
        moveLeft = new Animation(200, Assets.player_left);
        moveRight = new Animation(200, Assets.player_right);
        idle = new Animation(600, Assets.player_idle);
        die = new Animation(400, Assets.player_die);
    }

    public void tick() {
        if (!died) {
            up = handler.getKeyManager().isPressed(KeyEvent.VK_W);
            down = handler.getKeyManager().isPressed(KeyEvent.VK_S);
            right = handler.getKeyManager().isPressed(KeyEvent.VK_D);
            left = handler.getKeyManager().isPressed(KeyEvent.VK_A);
            idle.tick();
            moveUp.tick();
            moveDown.tick();
            moveLeft.tick();
            moveRight.tick();
            checkCollideWithEnemy();
            updateSpeed();
            move();
            handler.getGameCamera().centerOnCreature((int) x, (int) y, width, height);
            if (handler.getKeyManager().keyJustPressed(KeyEvent.VK_SPACE)) {
                addEntity(new Bomb(handler, (int) x + (bounds.x + bounds.width - bounds.height + 20) / 2, (int) y + (bounds.y + bounds.height) / 2));
            }
        } else {
            die.tick();
            if (die.isTimeout()) {
                shouldRemove = true;
                JFrame frame = handler.getGame().getDisplay().getFrame();
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        }
        if(!shouldRemove) updateFrame();
    }

    public void updateFrame() {
        if (died) {
            texture = die.getCurrentFrame();
        } else if (up) {
            texture = moveUp.getCurrentFrame();
        } else if (down) {
            texture = moveDown.getCurrentFrame();
        } else if (left) {
            texture = moveLeft.getCurrentFrame();
        } else if (right) {
            texture = moveRight.getCurrentFrame();
        } else {
            texture = idle.getCurrentFrame();
        }
    }

    public void updateSpeed() {
        xMove = 0;
        yMove = 0;
        if (up) {
            yMove = -speed;
        } else if (down) {
            yMove = speed;
        } else if (left) {
            xMove = -speed;
        } else if (right) {
            xMove = speed;
        }
    }

    public void render(Graphics g) {
        g.drawImage(texture, (int) x - handler.getGameCamera().getxOffset(), (int) y - handler.getGameCamera().getyOffset(), width, height, null);
    }


}
