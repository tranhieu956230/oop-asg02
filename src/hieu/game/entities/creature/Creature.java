package hieu.game.entities.creature;

import hieu.game.Handler;
import hieu.game.entities.Entity;
import hieu.game.tiles.Tile;


public abstract class Creature extends Entity {
    private final int DEFAULT_HEALTH = 10;
    private final int DEFAULT_DAMAGE = 2;
    private final int DEFAULT_SPEED = 5;
    protected int speed, health, damage;
    protected boolean died = false, shouldRemove = false;


    public Creature(Handler handler, float x, float y, int width, int height, int priority) {
        super(handler, x, y, width, height, priority);
        health = DEFAULT_HEALTH;
        damage = DEFAULT_DAMAGE;
        speed = DEFAULT_SPEED;
    }

    public int getDamage() {
        return damage;
    }

    public void hurt(int damage) {
        if (!died) {
            health -= damage;
            if (health < 0) {
                died = true;
            }
        }
    }

    public void move() {
        if (!died) {
            if (!isCollided()) moveX();
            else if (this instanceof Bazooka) {
                ((Bazooka) this).changeDirection();
            }
            if (!isCollided()) moveY();
            else if (this instanceof Bazooka) {
                ((Bazooka) this).changeDirection();
            }
        }
    }

    public void moveX() {
        if (xMove > 0) {
            if (!handler.getWorld().getTile((int) (x + bounds.x + bounds.width + xMove) / Tile.TILE_WIDTH, (int) (y + bounds.y) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x + bounds.x + bounds.width + xMove) / Tile.TILE_WIDTH, (int) (y + bounds.y + bounds.height) / Tile.TILE_HEIGHT).isSolid())
                x += xMove;
            else {
                x = ((int) (x + bounds.x + bounds.width + xMove) / Tile.TILE_WIDTH) * Tile.TILE_WIDTH - bounds.x - bounds.width - 1;
                if (this instanceof Bazooka) {
                    ((Bazooka) this).changeDirection();
                }
            }
        } else if (xMove < 0) {
            if (!handler.getWorld().getTile((int) (x + bounds.x + xMove) / Tile.TILE_WIDTH, (int) (y + bounds.y) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x + bounds.x + xMove) / Tile.TILE_WIDTH, (int) (y + bounds.y + bounds.height) / Tile.TILE_HEIGHT).isSolid())
                x += xMove;
            else {
                x = ((int) (x + bounds.x) / Tile.TILE_WIDTH) * Tile.TILE_WIDTH - bounds.x + 1;
                if (this instanceof Bazooka) {
                    ((Bazooka) this).changeDirection();
                }
            }
        }
    }

    public void moveY() {
        if (yMove < 0) {
            if (!handler.getWorld().getTile((int) (x + bounds.x) / Tile.TILE_WIDTH, (int) (y + bounds.y + yMove) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x + bounds.x + bounds.width) / Tile.TILE_WIDTH, (int) (y + bounds.y + yMove) / Tile.TILE_HEIGHT).isSolid())
                y += yMove;
            else {
                y = ((int) (y + bounds.y) / Tile.TILE_HEIGHT) * Tile.TILE_HEIGHT - bounds.y + 1;
                if (this instanceof Bazooka) {
                    ((Bazooka) this).changeDirection();
                }
            }
        } else if (yMove > 0) {
            if (!handler.getWorld().getTile((int) (x + bounds.x) / Tile.TILE_WIDTH, (int) (y + bounds.y + bounds.height + yMove) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x + bounds.x + bounds.width) / Tile.TILE_WIDTH, (int) (y + bounds.y + bounds.height + yMove) / Tile.TILE_HEIGHT).isSolid())
                y += yMove;
            else {
                y = ((int) (y + bounds.y + bounds.height + yMove) / Tile.TILE_HEIGHT) * Tile.TILE_HEIGHT - bounds.y - bounds.height - 1;
                if (this instanceof Bazooka) {
                    ((Bazooka) this).changeDirection();
                }
            }
        }
    }

    public boolean isShouldRemove() {
        return shouldRemove;
    }


}
