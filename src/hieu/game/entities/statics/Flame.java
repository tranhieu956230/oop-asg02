package hieu.game.entities.statics;

import hieu.game.Handler;
import hieu.game.entities.Entity;
import hieu.game.gfx.Animation;
import hieu.game.gfx.Assets;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Flame extends Entity {
    private Animation flame;
    private BufferedImage texture;
    private long duration = 2000000000L;
    private long lastTime;
    public Flame(Handler handler, int x, int y) {
        super(handler, x, y, Assets.bombWidth, Assets.bombHeight, 1);
        lastTime = System.nanoTime();
        flame = new Animation(100, Assets.flame);
        int xBound = 0;
        int yBound = 0;
        int boundWidth = width;
        int boundHeight = height;
        bounds = new Rectangle(xBound, yBound, boundWidth, boundHeight);
    }

    public boolean isTimeOut() {
        if(System.nanoTime() - lastTime > duration) return true;
        return false;
    }

    @Override
    public void tick() {
        flame.tick();
        texture = flame.getCurrentFrame();
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(texture, (int) x - handler.getGameCamera().getxOffset(), (int) y - handler.getGameCamera().getyOffset(), width, height, null);
    }
}
