package hieu.game.entities.statics;

import hieu.game.Handler;
import hieu.game.entities.Entity;
import hieu.game.gfx.Animation;
import hieu.game.gfx.Assets;
import hieu.game.tiles.Tile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Bomb extends Entity {
    private static ArrayList<Bomb> bombs = new ArrayList<>();
    private BufferedImage texture;
    private Animation exploding;
    private long explodeTime = 3000000000L;
    private long lastTime;
    public static final int damage = 3;

    public Bomb(Handler handler, int x, int y) {
        super(handler, x, y, Assets.bombWidth, Assets.bombHeight, 1);
        exploding = new Animation(200, Assets.bomb);
        lastTime = System.nanoTime();
        int xBound = 0;
        int yBound = 0;
        int boundWidth = width;
        int boundHeight = height;
        bounds = new Rectangle(xBound, yBound, boundWidth, boundHeight);
    }

    public int getDamage() {
        return damage;
    }

    public void explode() {
        addEntity(new Flame(handler, (int) (x), (int) y));

        for (float i = 1; i < 5; i++) {
            if (!handler.getWorld().getTile((int) (x + (i) * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x + i * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + Assets.bombHeight) / Tile.TILE_HEIGHT).isSolid())
                addEntity(new Flame(handler, (int) (x + i * Assets.bombWidth), (int) y));
            else {
                if (handler.getWorld().getTile((int) (x + (i) * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x + (i) * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y) / Tile.TILE_HEIGHT, 22);
                if (handler.getWorld().getTile((int) (x + i * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + Assets.bombHeight) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x + i * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + Assets.bombHeight) / Tile.TILE_HEIGHT, 22);
                break;
            }
        }
        for (float i = 1; i < 5; i++) {
            if (!handler.getWorld().getTile((int) (x - (i) * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x - i * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + Assets.bombHeight) / Tile.TILE_HEIGHT).isSolid())
                addEntity(new Flame(handler, (int) (x - i * Assets.bombWidth), (int) y));
            else {
                if (handler.getWorld().getTile((int) (x - (i) * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x - (i) * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y) / Tile.TILE_HEIGHT, 22);
                if (handler.getWorld().getTile((int) (x - i * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + Assets.bombHeight) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x - i * Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + Assets.bombHeight) / Tile.TILE_HEIGHT, 22);
                break;
            }
        }
        for (float i = 1; i < 5; i++) {
            if (!handler.getWorld().getTile((int) (x) / Tile.TILE_WIDTH, (int) (y + i * Assets.bombHeight) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x + Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + i * Assets.bombHeight) / Tile.TILE_HEIGHT).isSolid())
                addEntity(new Flame(handler, (int) (x), (int) (y + i * Assets.bombWidth)));
            else {
                if (handler.getWorld().getTile((int) (x) / Tile.TILE_WIDTH, (int) (y + i * Assets.bombHeight) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x) / Tile.TILE_WIDTH, (int) (y + i * Assets.bombHeight) / Tile.TILE_HEIGHT, 22);
                if (handler.getWorld().getTile((int) (x + Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + i * Assets.bombHeight) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x + Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y + i * Assets.bombHeight) / Tile.TILE_HEIGHT, 22);
                break;
            }
        }
        for (float i = 1; i < 5; i++) {
            if (!handler.getWorld().getTile((int) (x) / Tile.TILE_WIDTH, (int) (y - i * Assets.bombHeight) / Tile.TILE_HEIGHT).isSolid() &&
                    !handler.getWorld().getTile((int) (x + Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y - i * Assets.bombHeight) / Tile.TILE_HEIGHT).isSolid())
                addEntity(new Flame(handler, (int) (x), (int) (y - i * Assets.bombWidth)));
            else {
                if (handler.getWorld().getTile((int) (x) / Tile.TILE_WIDTH, (int) (y - i * Assets.bombHeight) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x) / Tile.TILE_WIDTH, (int) (y - i * Assets.bombHeight) / Tile.TILE_HEIGHT, 22);
                if (handler.getWorld().getTile((int) (x + Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y - i * Assets.bombHeight) / Tile.TILE_HEIGHT).isBreakable())
                    handler.getWorld().setTile((int) (x + Assets.bombWidth) / Tile.TILE_WIDTH, (int) (y - i * Assets.bombHeight) / Tile.TILE_HEIGHT, 22);
                break;
            }
        }
    }

    @Override
    public void tick() {
        exploding.tick();
        texture = exploding.getCurrentFrame();
    }

    public boolean isTimeOut() {
        if (System.nanoTime() - lastTime > explodeTime) return true;
        return false;
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(texture, (int) x - handler.getGameCamera().getxOffset(), (int) y - handler.getGameCamera().getyOffset(), width, height, null);
    }
}
