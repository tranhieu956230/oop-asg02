package hieu.game;

import hieu.game.gfx.GameCamera;
import hieu.game.input.KeyManager;
import hieu.game.world.World;

public class Handler {
    private Game game;
    private World world;
    private KeyManager keyManager;
    private GameCamera gameCamera;

    public Handler(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public World getWorld() {
        return world;
    }

    public void setKeyManager(KeyManager keyManager) {
        this.keyManager = keyManager;
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    public void setGameCamera(GameCamera camera){
        this.gameCamera = camera;
    }

    public GameCamera getGameCamera() {
        return gameCamera;
    }


}
