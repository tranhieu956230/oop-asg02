package hieu.game.states;

import hieu.game.Handler;
import hieu.game.world.World;

import java.awt.*;

public class GameState extends State{
    private World world;

    public GameState(Handler handler) {
        super(handler);
        world = new World(handler, "res/worlds/world2.txt");
        handler.setWorld(world);
    }

    @Override
    public void tick() {
        world.tick();
    }

    @Override
    public void render(Graphics g) {
        world.render(g);
    }
}
