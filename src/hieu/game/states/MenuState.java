package hieu.game.states;

import hieu.game.Handler;
import hieu.game.gfx.Assets;
import hieu.game.input.KeyManager;
import hieu.game.ui.UIManager;

import java.awt.*;
import java.awt.event.KeyEvent;

public class MenuState extends State {
    public MenuState(Handler handler) {
        super(handler);

    }

    @Override
    public void tick() {
    }

    public void drawString(Graphics g, String text, int xPos, int yPos, boolean center, Color c, Font font) {
        g.setColor(c);
        g.setFont(font);
        int x = xPos;
        int y = yPos;
        if(center) {
            FontMetrics fm = g.getFontMetrics(font);
            x = xPos - fm.stringWidth(text) / 2;
            y = (yPos - fm.getHeight() /2) + fm.getAscent();
        }
        g.drawString(text, x, y);
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.background, 0, 0, handler.getGame().getDisplay().getWidth(), handler.getGame().getDisplay().getHeight(), null);
        g.drawImage(Assets.title, 0, 0, handler.getGame().getDisplay().getWidth(), handler.getGame().getDisplay().getHeight(), null);
        drawString(g, "Press Enter to play", handler.getGame().getDisplay().getWidth()/2, handler.getGame().getDisplay().getHeight()/2, true, Color.WHITE, Assets.font28);
    }
}
